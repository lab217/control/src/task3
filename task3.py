
from random import random
import numpy as np
import matplotlib.pyplot as plt

trials = list(np.linspace(10, 1000000, 1000))  # Генератор значений
pi = []#Массив чисео P


def mc_multiple_runs(trials, hits=0):
   #Возвращает вхождения в функцию метода МК
    for i in range(int(trials)):#Цикл по генератору
        x, y = random(), random()  # Генерация случайных значений по х у в каждом запуске(проходе if)

        if x ** 2 + y ** 2 < 1:  # Определение значения
            hits = hits + 1#Подсчёт вхождений
    return float(hits)


for i in trials: #Цикл испытаний
    pi.append(4 * (mc_multiple_runs(i) / i))#Запись значения пи как умноженное произведения попадания, делённого на испытание и умноженное на4
    print('Вхождения : %d, Испытания: %d, Полученное π = %1.4F' % (mc_multiple_runs(i), i, 4 * (mc_multiple_runs(i) / i)))

# plot graphs
plt.plot(trials, pi, 'g',  label='Испытания')
plt.axhline(y=3.1415 ,label = 'Идеальное значение')#Идеальное число π
plt.title('Оценка значения π методом Монте-Карло')
plt.xlabel('Количество испытаний')
plt.ylabel('Значения числа π')
plt.ylim(3.12, 3.17)#Ограничение оси y
plt.grid()
plt.legend()
plt.show()

plt.hist(pi, bins=np.linspace(3.12, 3.16, 50), color='blue')
plt.title('Оценка значения π методом Монте-Карло')
plt.xlabel('Расчетное значение пи')
plt.ylabel('Количество испытаний')
plt.xlim(3.13, 3.15)
plt.show()
plt.plot(pi, bins=np.linspace(3.12, 3.16, 50), color='blue')
plt.title('Оценка значения π методом Монте-Карло')
plt.xlabel('Расчетное значение π')
plt.ylabel('Количество испытаний')
plt.xlim(3.13, 3.15)
plt.show()